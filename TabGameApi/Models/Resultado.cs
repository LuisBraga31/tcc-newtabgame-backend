﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Resultado
    {
        public int Id { get; set; }
        public int Pontuacao { get; set; }
        public int AlunoId { get; set; }

    }
}
