﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Jogo
    {
        public int Id { get; set; }
        public string Titulo { get; set; }

        public List<JogoPergunta> JogosPerguntas { get; set; }
    }
}
