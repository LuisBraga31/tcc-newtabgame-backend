﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Partida
    {
        public int Id { get; set; }
        public string Codigo { get; set; }
        public bool State { get; set; }
        public int Rodada { get; set; }
        public string qtdJogadores { get; set; }
        public DateTime PartidaInicio  { get; set; }
        public DateTime PartidaFim { get; set; }
        public int JogoId { get; set; }
        public int ProfessorId { get; set; }

    }
}
