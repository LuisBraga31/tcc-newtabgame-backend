﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Resposta
    {
        public int PerguntaId { get; set; }
        public int ResultadoId { get; set; }
        public string EscolhaJogador { get; set; }
    }
    public enum Escolha
    {
        A,
        B,
        C,
        D
    }
}
