﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Pergunta
    {
        public int Id { get; set; }
        public string Tema { get; set; }
        public string Enunciado { get; set; }
        public int Dificuldade { get; set; }
        public int Tempo { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string RespCorreta { get; set; }
        public List<JogoPergunta>  JogosPerguntas { get; set; }
    }
    public enum Alternativa
    {
        A,
        B,
        C,
        D
    }
}
