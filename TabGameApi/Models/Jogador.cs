﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class Jogador
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int PartidaId { get; set; }

    }
}
