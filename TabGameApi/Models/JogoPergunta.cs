﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TabGameApi.Models
{
    public class JogoPergunta
    {
        public int JogoId { get; set; }
        public int PerguntaId { get; set; }
        public Pergunta Pergunta { get; set; }
        public Jogo Jogo { get; set; }
    }
}
