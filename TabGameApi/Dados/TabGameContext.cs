﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TabGameApi.Models;

namespace TabGameApi.Dados
{
    public class TabGameContext : DbContext
    {
        public TabGameContext(DbContextOptions<TabGameContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JogoPergunta>()
                .HasKey(ac => new { ac.JogoId, ac.PerguntaId });
            modelBuilder.Entity<Resposta>()
                .HasKey(ac => new { ac.PerguntaId, ac.ResultadoId});
           /* modelBuilder.Entity<Pergunta>()
                .Property(e => e.RespCorreta)
                .HasConversion(
                    v => v.ToString(),
                    v => (Alternativa)Enum.Parse(typeof(Alternativa), v));
            modelBuilder.Entity<Resposta>()
                .Property(e => e.EscolhaJogador)
                .HasConversion(
                    v => v.ToString(),
                    v => (Escolha)Enum.Parse(typeof(Escolha), v)); */
        }

        public DbSet<Pergunta> Perguntas { get; set; }
        public DbSet<Jogo> Jogos { get; set; }
        public DbSet<Partida> Partidas{ get; set; }
        public DbSet<Professor> Professores { get; set; }
        public DbSet<Jogador>  Jogadores { get; set; }
        public DbSet<Resultado> Resultados { get; set; }
        public DbSet<TabGameApi.Models.JogoPergunta> JogosPerguntas { get; set; }
        public DbSet<TabGameApi.Models.Resposta> Respostas { get; set; }


    }
}
