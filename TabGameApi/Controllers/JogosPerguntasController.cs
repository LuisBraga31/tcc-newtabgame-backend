﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TabGameApi.Dados;
using TabGameApi.Models;

namespace TabGameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JogosPerguntasController : ControllerBase
    {
        private readonly TabGameContext _context;

        public JogosPerguntasController(TabGameContext context)
        {
            _context = context;
        }

        // GET: api/JogosPerguntas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<JogoPergunta>>> GetJogosPerguntas()
        {
            return await _context.JogosPerguntas.ToListAsync();
        }

        // GET: api/JogosPerguntas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<JogoPergunta>> GetJogoPergunta(int id)
        {
            var jogoPergunta = await _context.JogosPerguntas.FindAsync(id);

            if (jogoPergunta == null)
            {
                return NotFound();
            }

            return jogoPergunta;
        }

        // PUT: api/JogosPerguntas/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJogoPergunta(int id, JogoPergunta jogoPergunta)
        {
            if (id != jogoPergunta.JogoId)
            {
                return BadRequest();
            }

            _context.Entry(jogoPergunta).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JogoPerguntaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JogosPerguntas
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<JogoPergunta>> PostJogoPergunta(JogoPergunta jogoPergunta)
        {
            _context.JogosPerguntas.Add(jogoPergunta);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (JogoPerguntaExists(jogoPergunta.JogoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetJogoPergunta", new { id = jogoPergunta.JogoId }, jogoPergunta);
        }

        // DELETE: api/JogosPerguntas/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<JogoPergunta>> DeleteJogoPergunta(int id)
        {
            var jogoPergunta = await _context.JogosPerguntas.FindAsync(id);
            if (jogoPergunta == null)
            {
                return NotFound();
            }

            _context.JogosPerguntas.Remove(jogoPergunta);
            await _context.SaveChangesAsync();

            return jogoPergunta;
        }

        private bool JogoPerguntaExists(int id)
        {
            return _context.JogosPerguntas.Any(e => e.JogoId == id);
        }
    }
}
