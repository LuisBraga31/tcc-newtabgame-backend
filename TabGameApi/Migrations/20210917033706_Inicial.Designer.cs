﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using TabGameApi.Dados;

namespace TabGameApi.Migrations
{
    [DbContext(typeof(TabGameContext))]
    [Migration("20210917033706_Inicial")]
    partial class Inicial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.10")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("TabGameApi.Models.Jogador", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Nickname")
                        .HasColumnType("text");

                    b.Property<int>("PartidaId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Jogadores");
                });

            modelBuilder.Entity("TabGameApi.Models.Jogo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Titulo")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Jogos");
                });

            modelBuilder.Entity("TabGameApi.Models.JogoPergunta", b =>
                {
                    b.Property<int>("JogoId")
                        .HasColumnType("integer");

                    b.Property<int>("PerguntaId")
                        .HasColumnType("integer");

                    b.HasKey("JogoId", "PerguntaId");

                    b.HasIndex("PerguntaId");

                    b.ToTable("JogosPerguntas");
                });

            modelBuilder.Entity("TabGameApi.Models.Partida", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Codigo")
                        .HasColumnType("text");

                    b.Property<int>("JogoId")
                        .HasColumnType("integer");

                    b.Property<DateTime>("PartidaFim")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("PartidaInicio")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("ProfessorId")
                        .HasColumnType("integer");

                    b.Property<string>("qtdJogadores")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Partidas");
                });

            modelBuilder.Entity("TabGameApi.Models.Pergunta", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("Dificuldade")
                        .HasColumnType("integer");

                    b.Property<string>("Enunciado")
                        .HasColumnType("text");

                    b.Property<string>("OptionA")
                        .HasColumnType("text");

                    b.Property<string>("OptionB")
                        .HasColumnType("text");

                    b.Property<string>("OptionC")
                        .HasColumnType("text");

                    b.Property<string>("OptionD")
                        .HasColumnType("text");

                    b.Property<string>("RespCorreta")
                        .HasColumnType("text");

                    b.Property<string>("Tema")
                        .HasColumnType("text");

                    b.Property<int>("Tempo")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Perguntas");
                });

            modelBuilder.Entity("TabGameApi.Models.Professor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Email")
                        .HasColumnType("text");

                    b.Property<string>("Nascimento")
                        .HasColumnType("text");

                    b.Property<string>("Nome")
                        .HasColumnType("text");

                    b.Property<string>("Senha")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Professores");
                });

            modelBuilder.Entity("TabGameApi.Models.Resposta", b =>
                {
                    b.Property<int>("PerguntaId")
                        .HasColumnType("integer");

                    b.Property<int>("ResultadoId")
                        .HasColumnType("integer");

                    b.Property<string>("EscolhaJogador")
                        .HasColumnType("text");

                    b.HasKey("PerguntaId", "ResultadoId");

                    b.ToTable("Respostas");
                });

            modelBuilder.Entity("TabGameApi.Models.Resultado", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("AlunoId")
                        .HasColumnType("integer");

                    b.Property<int>("Pontuacao")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Resultados");
                });

            modelBuilder.Entity("TabGameApi.Models.JogoPergunta", b =>
                {
                    b.HasOne("TabGameApi.Models.Jogo", "Jogo")
                        .WithMany("JogosPerguntas")
                        .HasForeignKey("JogoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("TabGameApi.Models.Pergunta", "Pergunta")
                        .WithMany("JogosPerguntas")
                        .HasForeignKey("PerguntaId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Jogo");

                    b.Navigation("Pergunta");
                });

            modelBuilder.Entity("TabGameApi.Models.Jogo", b =>
                {
                    b.Navigation("JogosPerguntas");
                });

            modelBuilder.Entity("TabGameApi.Models.Pergunta", b =>
                {
                    b.Navigation("JogosPerguntas");
                });
#pragma warning restore 612, 618
        }
    }
}
